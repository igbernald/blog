@extends('layouts.app')
<link rel="icon" type="image/png" href="{{ asset('images/icons/platillos.svg') }}" rel="stylesheet">
@section('title', 'Platillo')
@section('content')
<div class="">
    <div class="div-search-index">
        <form class="form-inline my-2 my-lg-0" style="float: right;">
            <a href="{{url('saucer/create')}}" class="btn btn-primary mr-sm-2 input-small"><i class="fa-fw fa-plus fa"></i></a>
            <input class="form-control-new mr-sm-2 input-small" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-success my-2 btn-sm" type="submit">Search</button>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" >Titulo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
               
            </tbody>
        </table>
    </div>
</div>
<div class="pagination-new">
    
</div>
@endsection
