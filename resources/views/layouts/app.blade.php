<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Scripts New Functions -->
    <script src="{{ asset('js/newFunctions.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Font Icons -->
    <script src="https://kit.fontawesome.com/4c93727823.js" crossorigin="anonymous"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/newStyles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebarPlantilla.css') }}" rel="stylesheet">
    <!-- Sweetalert2 -->
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.js') }}"></script>
</head>
<body class="skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="/saucer" class="logo">
                <span class="logo-mini"><b>BLOG</b></span>
                <span class="logo-lg"><b>Blog Usuarios</b></span>
            </a>
            <nav class="navbar" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/users/avatar5.png" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="images/users/avatar5.png" class="img-circle user-image" alt="User Image">
                                    <p>
                                        {{ Auth::user()->name }}
                                        <small style="font-size: 10px; font-style: italic; color: white;">{{ Auth::user()->email }}</small>
                                        <small>Administrador</small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-dark btn-sm">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-dark btn-sm" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="images/users/avatar5.png" class="user-image" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{{ Auth::user()->name }}</p>
                        <p style="font-size:11px; color: #00a65a;">Administrador</p>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    
                    <li><a href="{{url('/home')}}"><i class="fas fa-users"></i> <span>Entradas</span></a></li>
                    <li><a href="{{url('/home')}}"><i class="fas fa-users"></i> <span>Usuarios</span></a></li>

                    {{-- <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Link in level 2</a></li>
                            <li><a href="#">Link in level 2</a></li>
                        </ul>
                    </li> --}}
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content container-fluid-new">
                <main class="container-new-main">
                    @yield('content')
                </main>
            </section>
        </div>
    </div>
</body>
</html>
