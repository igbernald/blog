<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Laravel</title>

            <!-- Fonts -->
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="../css/app.css">
            <!-- Styles -->
            <link href="{{ asset('css/newStyles.css') }}" rel="stylesheet">
        </head>
        <body class="container-welcome">
            <div class="position-ref full-height">
                @if (Route::has('login'))
                <div class="top-right links" style="float: right; margin-right: 15px; font-size: 21px;">
                    @auth
                    <a href="{{ url('/home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}" style="color: black; padding-right: 20px;">Login</a>

                    @if (Route::has('register'))
                    <a href="{{ route('register') }}" style="color: black; padding-right: 20px;">Register</a>
                    @endif
                    @endauth
                </div>
                @endif
                <div class="content">
                    <div class="title m-b-md">
                        <h2>Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card" >
                        <img src="images/users/avatar5.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>                
                            <img src="images/users/avatar5.png" class="rounded-circle" alt=30*30 style="width: 90px; height: 85px">
                            <h4 class="h4-card-welcome"> Titulo autor</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card" >
                        <img src="images/users/avatar5.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>                
                            <img src="images/users/avatar5.png" class="rounded-circle" alt=30*30 style="width: 90px; height: 85px">
                            <h4 class="h4-card-welcome"> Titulo autor</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card" >
                        <img src="images/users/avatar5.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>                
                            <img src="images/users/avatar5.png" class="rounded-circle" alt=30*30 style="width: 90px; height: 85px">
                            <h4 class="h4-card-welcome"> Titulo autor</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card" >
                        <img src="images/users/avatar5.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>                
                            <img src="images/users/avatar5.png" class="rounded-circle" alt=30*30 style="width: 90px; height: 85px">
                            <h4 class="h4-card-welcome"> Titulo autor</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
